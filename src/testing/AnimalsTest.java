package testing;

import static org.junit.Assert.*;

import org.junit.Test;

public class AnimalsTest {

	@Test
	public void test() {
		AnimalsReport ar = new AnimalsReport();
		
		ar.dogs=50;
		ar.cats=40;
		
		ar.displayStatistics();
		System.out.println("There are " + ar.getAnimalsCount() + " animals");
		
	}

}
