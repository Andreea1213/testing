package testing;

import static org.junit.Assert.*;

import org.junit.Test;

public class StudentTest {

	@Test
	public void testSetName() {
		Student student = new Student();
		student.setName("Maria");
		assertEquals(student.getName(), "Maria");
	}

	@Test
	public void testSetEmail() {
		Student student = new Student();
		student.setEmail("test");
		assertTrue(student.getEmail() == "test");  //sau assertEquals
	}
}
